# f2/asserty

Unit testing for lazy people. When it is simple, it gets done.

I developed this tool because phpunit is very verbose and there is a lot of writing to do for almost
everything you wish to test. This may be suitable for large organizations, but many projects are really
small.

A minimal example test;

```php
<?php
require('vendor/autoload.php');

// The simplest tests are for something "truthy":
F2\assert(file_exists(__DIR__.'/../composer.lock'), "Did you delete composer.lock?");


// Exceptions and errors can be tested inside a closure:
F2\expect(\Exception::class, function() {

    $api = new SomeAPI();

}, "Expected an exception");

F2\expect(E_NOTICE, function() {

    file_get_contents('file-that-does-not-exist');

});


```


## Installation

It's a composer package... Use `--dev` to avoid loading asserty in your production environment.

```bash
# composer require --dev f2/asserty
```


## Writing a test

Create a file `./tests/001-example-tests.php`

```php
<?php
require('vendor/autoload.php');

use function F2/asserty, F2/expect;

/**
 * These will not cause errors and will not output anything
 */

// All are truthy
asserty(1 > 0);                             
asserty(is_string("Asserty is simple"));
asserty(![], "Empty arrays are falsey, so this is fine");

// Optional error description
asserty(true, 'Something is really wrong here...');

// Optional exit code
asserty(true, 'Something is really wrong here...', 10);

/**
 * These will cause errors
 */

// Unless you specify null as the exit code, the tests will stop after the first error is encountered
asserty(false, "False is considered an error condition", null);

asserty([], "Empty arrays are falsey", null);
?>
```


## Run ALL tests in ./tests/

```bash
# ./vendor/bin/asserty
`

## Run a single test

```bash
# php tests/001-my-tests.php
```







asserty(is_int(123));
asserty







## Quick example

Create a test.php file:

```php
<?php
require('vendor/autoload.php');
use function F2\asserty;
asserty("A" > "O", "In this land; Alpha is greater than Omega");
```

Run the file:

```shell
$ php test.php
[F2\asserty] In this land, Alpha is greater than Omega
```

## Batch testing

f2/asserty comes with a command line tool for doing batch testing.

```shell
$ ./vendor/bin/asserty
```

The command will then look for a folder named 'tests/' in the current
directory and start iterating over all php files there.

```shell
$ ./vendor/bin/asserty
[ERROR] tests/asserty-failed.php | [F2\asserty] In this land, Alpha is greater than Omega
[OK]    tests/nothing-wrong.php  | Good job!
[ERROR] tests/sub-folder/foo.php | PHP Parse error:  syntax error, unexpected end of file in /var/www/fubber2/PACKAGES/asserty/tests/sub-folder/foo.php on line 3
[F2\asserty] There were 2 mistakes
```

## Exit Codes

Asserty will emit non-zero exit codes if tests fail, so with some
shell scripting you can easily automate stuff.

To execute a command if testing succeeds:

```shell
$ ./vendor/bin/asserty && echo "FOUL PLAY"
```

To execute a command if everything as perfect:

```shell
$ ./vendor/bin/asserty || echo "Good"
```
