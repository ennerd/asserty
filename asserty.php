<?php
namespace F2;

use function restore_error_handler, restore_exception_handler, set_exception_handler, set_error_handler, debug_backtrace;

$GLOBALS["f2"]["asserty"]["runner"] = false;
$GLOBALS['f2']['asserty']['stack'] = [];
$GLOBALS['f2']['asserty']['tests'] = [];

if (getenv("ASSERTY_RUNNER") && strpos(getenv("ASSERTY_RUNNER"), sys_get_temp_dir())===0) {
    $GLOBALS["f2"]["asserty"]["runner"] = true;
    register_shutdown_function(function() {
        $fp = fopen(getenv("ASSERTY_RUNNER"), "wb");
        fwrite($fp, serialize($GLOBALS["f2"]));
        fclose($fp);
    });
}

/**
 * Checks that $truthy is not falsey, for if not, it will $else and $exitCode.
 *
 * @see https://www.php.net/assert
 *
 * Example:
 * `\F2\asserty('A' > 'B', 'A must come before B in backward land');`
 *
 * @param mixed $truthy     Is a boolean expression. If it evaluates to false, everything is fine.
 * @param mixed $else       Is something that happens if the assertion fails. 
 * @param int $exitCode     Terminates the script with exit code $exitCode. Set to negative value if yo don't want script to terminate on errors
 */
function asserty($truthy, $else=null, $exitCode=1) {
    $result = [];
    $GLOBALS["f2"]["asserty"]["tests"][] = &$result;
/*
    if (sizeof($GLOBALS['f2']['asserty']['stack'])) {
            $last = array_pop($GLOBALS['f2']['asserty']['stack']);
            return asserty(false, 'Unexpected '.$last[1].' code='.$last[0], 255);
    }
*/

    if(!$truthy) {
        $result["error"] = true;
        if ($else === null) {
            $prev = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1)[0];
            fwrite(STDERR, '[F2\asserty] '.($result["message"] = 'Failed assertion at '.$prev['file'].":".$prev['line'])."\n");
        } else if(is_a($else, \Throwable::class)) {
            throw $else;
        } else if(is_callable($else)) {
            fwrite(STDERR, '[F2\asserty] '.($result["message"] = 'Failed assertion at '.$prev['file'].":".$prev['line'])."\n");
            $result["message"] = call_user_func($else);
        } else {
            fwrite(STDERR, '[F2\asserty] '.($result["message"] = $else)."\n");
        }

        if(is_int($exitCode) && $exitCode >= 0) {
            exit($exitCode);
        }
    } else {
        $result["error"] = false;
    }
}

/**
 * Fails if function cause any errors or exceptions
 *      expect(null, function() {})
 *
 * Success if function throws FileNotFoundException
 *      expect('FileNotFoundException', function() {});
 *      expect(new FileNotFoundException(), function() {});
 *
 * Success if function triggets an error code 42 or an exception with code 42
 *      expect(42, function() {});
 *
 * Success if function throws any exception
 *      expect(\Throwable::class, function() {});
 */
function expect($expected, callable $callable, $else=null, $exitCode=1) {
    $GLOBALS['f2']['asserty']['stack'][] = [$expected, $else, $exitCode];

    set_error_handler($errorHandler = function($code, $message) use($expected, $else, $exitCode) {
        if ($expected === null) {
            return asserty(false, $else ?? "No error expected: '$message' code=$code", $exitCode);
        } else {
            return asserty($expected === $code || $expected === $message, $else ?? "Expected '$expected': '$message' code=$code", $exitCode);
        }
    });
    set_exception_handler($exceptionHandler = function($e) use ($expected, $else, $exitCode) {
        asserty(
            $expected === $e->getCode() ||                            // Code was right
            $expected === $e->getMessage() ||                         // Message was right
            $expected === get_class($e) ||                            // Compare namespace
            (                                                       // Expecting an object:
                is_object($expected) &&
                (
                    $expected === $e ||                               // The same object
                    get_class($expected) === get_class($e) ||         // The same class
                    $e instanceof $expected                           // Extends the class
                )
            ),
            $else ?? "Expected '$expected', got '$e'", $exitCode);
    });

    try {
        call_user_func($callable);
    } catch (\Throwable $e) {
        $exceptionHandler($e);
    }

    restore_error_handler();
    restore_exception_handler();
}
