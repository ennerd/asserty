<?php
require(__DIR__.'/../../vendor/autoload.php');

use function F2\expect;

foreach([null, E_ERROR, E_PARSE, E_NOTICE, E_CORE_ERROR, E_CORE_WARNING, E_COMPILE_ERROR, E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE, E_RECOVERABLE_ERROR] as $code) {
    expect($code, function() {
        file_get_contents("path-that-does not exist");
    }, null, -1);
}
